+++
active = true
date_format = "Jan 2006"
headless = true
subtitle = ""
title = "Experience"
weight = 40
widget = "experience"

[[experience]]
company = "Universidade Federal do Agreste de Pernambuco"
company_url = "http://www.ufape.edu.br"
date_end = ""
date_start = "2011-04-07"
description = "  Responsibilities include:\n  \n  * Cryptography and Network Security\n  * Blockchain and Cryptocurrencies Solutions\n  * System Design and Analysis\n  * Programming Languages\n  "
location = "Garanhuns, PE, Brasil"
title = "Professor"

[[experience]]
company = "Faculdade Metropolitana da Grande Recife"
company_url = "https://www.metropolitana.edu.br"
date_end = "2011-04-06"
date_start = "2008-08-01"
description = "Taught information technology, finance, linear programming and programming matter."
location = "Jaboatão dos Guararapes, PE, Brasil"
title = "Professor"

[[experience]]
company = "Faculdade Decisão"
company_url = "https://www.fade.edu.br"
date_end = "2009-04-30"
date_start = "2008-07-01"
description = "Taught Statistics."
location = "Paulista, PE, Brasil"
title = "Professor"

[[experience]]
company = "Universidade Federal de Pernambuco"
company_url = "https://www.ufpe.br"
date_end = "2011-04-06"
date_start = "2005-01-01"
description = "  Responsibilities include:\n  \n  * Electronic Procurement Management\n  * Member of the Pharmacy and Therapeutics Committee\n  * Contract Management\n  * Head of the Planning Unit\n  "
location = "Recife, PE, Brasil"
title = "Manager"

+++
