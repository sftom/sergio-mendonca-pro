+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "SEBRAE/PE"
  organization_url = "https://pe.sebrae.com.br"
  title = "Formação de Mentores (10h)"
  url = "https://www.sympla.com.br/startup-way-federais-club---oficina-para-mentores__865867"
  certificate_url = "files/accomplishments/20200701_sebrae_startup_way_federais_club_formacao_mentores.pdf"
  date_start = "2020-06-03"
  date_end = "2020-07-01"
  description = ""
#  description = "Python para Análise de Dados aborda desde o básico da linguagem, POO, Banco de Dados (Relacional e NoSQL), Numpy, Pandas, Matplotlib, Scikit-Learn, Análise Exploratória, TensorFlow, Modelo de Regressão, Machine Learning, Redes Neurais Artificiais, Deep Learning, Flask, Web Scraping..."

[[item]]
  organization = "Portal Embarcados"
  organization_url = "https://www.embarcados.com.br/"
  title = "Seminário de Sistemas Embarcados e IoT 2020: Edição Online (24h)"
  url = "https://www.sympla.com.br/seminario-de-sistemas-embarcados-e-iot-2020-edicao-online__852093#info"
  certificate_url = "files/accomplishments/20200628_embarcados_seminarios.pdf"
  date_start = "2020-06-27"
  date_end = "2020-06-28"
  description = ""
#  description = "Python para Análise de Dados aborda desde o básico da linguagem, POO, Banco de Dados (Relacional e NoSQL), Numpy, Pandas, Matplotlib, Scikit-Learn, Análise Exploratória, TensorFlow, Modelo de Regressão, Machine Learning, Redes Neurais Artificiais, Deep Learning, Flask, Web Scraping..."

[[item]]
  organization = "Udemy"
  organization_url = "https://www.udemy.com/"
  title = "Certificação Cisco CCNA 200-301 Atualizado 2020 (56.5h)"
  url = "https://www.udemy.com/course/curso-completo-cisco-ccnav3-200-125/"
  certificate_url = "https://ude.my/UC-4cec252d-b9b0-4d27-a6cd-915ee96089f7/"
  date_start = "2020-05-26"
  date_end = "2020-06-08"
  description = ""
#  description = ""

[[item]]
  organization = "Cisco NetAcad"
  organization_url = "https://www.netacad.com"
  title = "Cybersecurity Essentials (30h)"
  url = "https://www.netacad.com/courses/security/cybersecurity-essentials"
  certificate_url = "files/accomplishments/20200602_cybersecurity_essentials_certificate.pdf"
  date_start = "2020-05-29"
  date_end = "2020-06-02"
  description = ""
#  description = "Get security controls for networks, servers and applications; Learn valuable security principals and how to develop compliant policies. Implement proper procedures for data confidentiality and availability; Develop critical thinking and problem-solving skills using real equipment and Cisco Packet Tracer. Level: Intermediate."

[[item]]
  organization = "Data Science Academy"
  organization_url = "https://www.datascienceacademy.com.br/"
  title = "Python Fundamentos para Análise de Dados (54h)"
  url = "https://www.datascienceacademy.com.br/course?courseid=python-fundamentos"
  certificate_url = "https://mycourse.app/dHmN7nG8rvtEHrLx6"
  date_start = "2020-05-25"
  date_end = "2020-05-30"
  description = ""
#  description = "Python para Análise de Dados aborda desde o básico da linguagem, POO, Banco de Dados (Relacional e NoSQL), Numpy, Pandas, Matplotlib, Scikit-Learn, Análise Exploratória, TensorFlow, Modelo de Regressão, Machine Learning, Redes Neurais Artificiais, Deep Learning, Flask, Web Scraping..."

[[item]]
  organization = "Hacking.Rio"
  organization_url = "https://hackingrio.com/"
  title = "Hacking Help COVID-19 (60h)"
  url = "https://hackingrio.com/hackinghelp/"
  certificate_url = "files/accomplishments/2020_05_hacking_rio_certificado_mentor.pdf"
  date_start = "2020-05-22"
  date_end = "2020-05-24"
  description = ""
#  description = "Mentor do Hacking.Help COVID-19, o primeiro Hackathon Online Global do Hacking.Rio, nas fases preparatórias, ambientação e hackathon."

[[item]]
  organization = "Cisco NetAcad"
  organization_url = "https://www.netacad.com"
  title = "Introduction to Cybersecurity (15h)"
  url = "https://www.netacad.com/courses/security/introduction-cybersecurity"
  certificate_url = "files/accomplishments/20200531_introduction_to_cybersecurity_certificate.pdf"
  date_start = "2020-04-01"
  date_end = "2020-04-28"
  description = ""
#  description = "Learn what cybersecurity is and its potential impact to you; Understand the most common threats, attacks and vulnerabilities; Gain insights for how businesses protect their operations from attacks; Find the latest job trends and why cybersecurity continues to grow. Level: Beginning"

[[item]]
  organization = "SENAI/SC"
  organization_url = "https://cursos.sesisenai.org.br"
  title = "Desenho Arquitetônico (14h)"
  url = "https://cursos.sesisenai.org.br/detalhes/desenho-arquitetonico/20969"
  certificate_url = "files/accomplishments/20200425_desenho_arquitetonico_certificado.pdf"
  date_start = "2020-04-04"
  date_end = "2020-04-25"
  description = ""
#  description = "Normas, as principais técnicas e a história dos projetos de arquitetura, materiais e representações gráficas de estruturas."

[[item]]
  organization = "SENAI/SP"
  organization_url = "https://cursos.sesisenai.org.br"
  title = "Competência Transversal - Metrologia (14h)"
  url = "https://cursos.sesisenai.org.br/detalhes/programa-competencias-transversais-metrologia/7215"
  certificate_url = "files/accomplishments/20200404_metrologia_certificado.pdf"
  date_start = "2020-04-04"
  date_end = "2020-04-04"
  description = ""
#  description = "Aplicação e interpretação das medidas na área da mecânica."

[[item]]
  organization = "SENAI/Nacional"
  organization_url = "https://cursos.sesisenai.org.br"
  title = "Desvendando a Indústria 4.0 (20h)"
  url = "https://cursos.sesisenai.org.br/detalhes/desvendando-a-industria-4-0/15753"
  certificate_url = "files/accomplishments/20200404_desvendando_a_industria_4_0_certificado.pdf"
  date_start = "2020-03-01"
  date_end = "2020-04-04"
  description = ""
#  description = "Apresentar a Indústria 4.0, propiciando a introdução ao tema e a obtenção da base conceitual das tecnologias habilitadoras que suportam a Indústria 4.0."

[[item]]
  organization = "SENAI/SP"
  organization_url = "https://eadsenaies.com.br/"
  title = "Desvendando a Blockchain (20h)"
  url = "https://eadsenaies.com.br/cursos-senai/desvendando-a-blockchain/"
  certificate_url = "files/accomplishments/20200404_desvendando_a_blockchain_certificado.pdf"
  date_start = "2020-04-04"
  date_end = "2020-04-04"
  description = ""
#  description = "Conceitos, componentes, e contextos e soluções em blockchain, hipóteses e casos de uso."

[[item]]
  organization = "Udemy"
  organization_url = "https://www.udemy.com/"
  title = "Aprendendo SVG do início ao avançado (5.5h)"
  url = "https://www.udemy.com/course/aprendendo-svg-do-inicio-ao-avancado/"
  certificate_url = "https://ude.my/UC-4GT02AZX/"
  date_start = "2017-11-10"
  date_end = "2018-01-17"
  description = ""
#  description = ""
  
+++
