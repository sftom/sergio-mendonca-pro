---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Estágio Supervisionado Obrigatório - Apresentação e Procedimentos"
event: "Semestre 2020.2"
event_url:
location: "Universidade Federal do Agreste de Pernambuco"
address:
  street:
  city: "Garanhuns"
  region: "PE"
  postcode:
  country: "Brasil"
summary:
abstract: "Apresentação e Procedimentos sobre o Estágio Supervisionado Obrigatório na Universidade Federal do Agreste de Pernambuco"

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2021-09-22T09:00:00-03:00
date_end: 2021-09-22T12:00:00-03:00
all_day: false

# Schedule page publish date (NOT event date).
publishDate: 2021-09-21T09:00:00-03:00

authors: [admin]
tags: []

# Is this a featured event? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com/sftom
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your event's folder or a URL.
url_slides: files/eso.handout.pdf

url_code:
url_pdf: 
url_video: https://drive.google.com/file/d/1u7I3Wpq5FxLitkYJ2WjA5KVuZb3ksrT8/view?usp=drive_web

# Markdown Slides (optional).
#   Associate this event with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
