---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Curricularização da Extensão - Bacharelado em Ciência da Computação"
event: "I Encontro da Curricularização da Extensão"
event_url:
location: "Universidade Federal do Agreste de Pernambuco"
address:
  street:
  city: "Garanhuns"
  region: "PE"
  postcode:
  country: "Brasil"
summary:
abstract: "Apresentação sobre as diretrizes para a promoção da curricularização da extensão no curso de Bacharelado em Ciência da Computação, Universidade Federal do Agreste de Pernambuco."

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2022-03-31T11:00:00-03:00
date_end: 2022-03-31T12:00:00-03:00
all_day: false

# Schedule page publish date (NOT event date).
publishDate: 2022-03-31T13:00:00-03:00

authors: [admin, 'Dimas Nascimento Filho']
tags: []

# Is this a featured event? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com/sftom
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your event's folder or a URL.
url_slides: files/curricularizacao_extensao.slides.pdf

url_code:
url_pdf: 
url_video: https://drive.google.com/file/d/1If02QiYZYkCcoo7LFaPUCc4NhO-4rKzB/view?usp=sharing

# Markdown Slides (optional).
#   Associate this event with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
