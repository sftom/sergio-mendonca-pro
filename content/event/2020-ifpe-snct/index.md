---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "Cibersegurança na automação residencial e nas casas inteligentes"
event: "Semana Nacional de Ciência e Tecnologia 2020"
event_url:
location: "Instituto Federal de Pernambuco"
address:
  street:
  city: "Afogados da Ingazeira"
  region:
  postcode:
  country:
summary:
abstract:

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2020-10-27T16:00:38-03:00
date_end: 2020-10-27T17:30:38-03:00
all_day: false

# Schedule page publish date (NOT event date).
publishDate: 2020-10-27T17:30:38-03:00

authors: [admin]
tags: []

# Is this a featured event? (true/false)
featured: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

# Optional filename of your slides within your event's folder or a URL.
url_slides: https://docs.google.com/presentation/d/1Gq2QtSrDzHlhc2zOSpcV66q_EsS6jlMrQffHiokJqfs/edit?usp=sharing

url_code:
url_pdf: 20201027_snct_ifpe_afogados_da_ingazeira.pdf
url_video:

# Markdown Slides (optional).
#   Associate this event with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: [blockchain]
---
