---
summary: Zenit Polar is a very way for kids to write encrypted letters (their
  secrecies) so that adults cannot understand.
draft: false
authors:
  - admin
logo: assets/images/icon.png
thumbnail: featured.png
title: Zenit Polar
subtitle: A basic way for kids to write encrypted letters
date: 2007-10-03T23:05:16.000Z
featured: true
twitter:
  creator: "@sftom"
  username: "@sftom"
  card: summary
tags:
  - kidding
  - coding
  - python
  - cryptography
image:
  placement: 1
  caption: Photo by [Chris Ried](https://unsplash.com/@cdr6934) on
    [Unsplash](https://unsplash.com)
  focal_point: Center
  preview_only: true
  alt_text: Photo by Chris Ried on Unsplash
  filename: featured.png
description: Zenit Polar is a very basic way for kids to write encrypted letters
  (their secrecies) so that adults cannot understand.
---
Zenit Polar is a very basic way for kids to write encrypted letters (their secrecies) so that adults cannot understand.

In this rudimentary technique of encoding we exchange the letters for their correspondent just below it. It works like a dictionary.

```python
zenitpolarZENITPOLAR
polarzenitPOLARZENIT
```

Next, we can see the Python code:

```python
#!/usr/bin/python
# --*-- coding: utf8 --*--
# file: zenit-polar.py
import string
# Zenit-Polar dictionary variables
a = 'zenitpolarZENITPOLAR'
b = 'polarzenitPOLARZENIT'
# Zenit-Polar translate table
translateTable = string.maketrans(a,b)
# our message to encode
message = 'I like to hang out with my friends'
# our encoded/decoded message
encodedDecodedMessage = string.translate(message,translateTable)
# let's look our encoded/decoded message
print(encodedDecodedMessage)
```

You may see this code working in [Zenit Polar App](https://zenit-polar.appspot.com/) on Google App Engine. For more information please see [Zenit-Polar article] on Wikipedia.

That's all folks! See ya soon! :D :D

[Zenit-Polar article]: https://pt.wikipedia.org/wiki/Zenit_Polar
