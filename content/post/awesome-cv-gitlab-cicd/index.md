---
title: Automate your Awesome-CV with XeLaTeX and Gitlab CI
subtitle: Continuous Integration for your Awesome-CV
date: 2020-06-27T16:09:13.671Z
summary: Automate your Awesome-CV with XeLaTeX and Gitlab CI (Continuous Integration).
draft: false
featured: false
tags:
  - coding
  - LaTeX
  - continuous integration
  - Gitlab
image:
  filename: featured.png
  focal_point: Smart
  preview_only: false
  caption: Image by [Me](https://sergio.mendonca.pro) on [Sérgio Mendonça's
    website](https://sergio.mendonca.pro/post/awesome-cv-gitlab-cicd)
---
### Gitlab CI

There are numerous tutorials and documentation to learn Gitlab CI/CD. It's enough you just google it. :p

Everyone who knows LaTeX will probably complain now because I could just use Overleaf to edit my CV in the browser. That's totally correct, but I want to use Gitlab CI to present you with a simple exercise on Gitlab Continuous Integration.

I forked the official Awesome-CV repository into my own Gitlab. Everything that builds your Awesome-CV automatically can be found in the `.gitlab-ci.yml` file.

```yml
pdf:
  image: thomasweise/texlive
  script:
    - cd examples
    - xelatex cv.tex
  artifacts:
    paths:
    - examples/cv.pdf
```

### Explanation

By adding this file into the root directory of the repository a GitLab runner will execute the defined tasks.

* `pdf` defines the name of the runner job. 

  * `image`: The runner starts the docker image that has texlive installed, labeled `thomasweise/texlive`;
  * Everything in `script` will be executed in the docker container. Just like we use XeLaTeX on our local machine;

    * The runner will switch to the `examples` folder and;
    * Execute `xelatex cv.tex`;
  * That will generate a PDF file - if no error occurred. 

    * To obtain the generated PDF file we have to define the file as an artifact. The `artifacts` can be downloaded into a zipfile or browse to `examples/cv.pdf`, if the job has finished successfully.

The jobs for my repository can be found [here](https://gitlab.com/sftom/Awesome-CV/-/jobs).

In the [README.md](https://gitlab.com/sftom/Awesome-CV/-/blob/master/README.md) the status of the pipeline is displayed and direct links to the artifacts can be found. Have a look at the official documentation for the badges.

```markdown
# My Awesome-CV

[![pipeline status](https://gitlab.com/sftom/Awesome-CV/badges/master/pipeline.svg)](https://gitlab.com/sftom/Awesome-CV/-/commits/master)

[Browse](https://gitlab.com/sftom/Awesome-CV/builds/artifacts/master/browse?job=pdf) the Artifacts or [download](https://gitlab.com/sftom/Awesome-CV/-/jobs/artifacts/master/raw/examples/cv.pdf?job=pdf) the PDF directly.

I forked from [Awesome-CV repository](https://github.com/posquit0/Awesome-CV) into my own space on GitLab.
```

[![](./readme.png)](https://gitlab.com/sftom/Awesome-CV/-/blob/master/README.md)

If you want to build your own Awesome-CV with Gitlab CI feel free to fork [my repository](https://gitlab.com/sftom/Awesome-CV/-/tree/customizing) and start editing the files in the *customizing* branch.

Adapted by [LARS LÜHR, Blogging about coding, security and photography](https://ayeks.de/post/2018-01-25-awesome-cv-cicd/).