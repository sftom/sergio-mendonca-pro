---
title: "How to create a profile on the new 'Github profile'"
subtitle: "An attractive Github profile"
date: 2020-07-12T17:12:43.536Z
summary: "Github launched a cool feature! A README.md Github profile"
draft: false
featured: false
tags:
  - github
  - profile
  - branding
  - markdown
image:
  filename: featured.gif
  focal_point: Smart
  preview_only: false
  caption: Image by [Me](https://github.com/sftom)
---
Github launched a cool feature!

The user can create a presentation page, through a README.md, within a public project of the same name as the user.

And, it should look like this:

[`https://github.com/sftom/sftom/README.md`](https://github.com/sftom/sftom/README.md)

Above, you can check my profile:

[`https://github.com/sftom`](https://github.com/sftom)
