---
title: 7-Segment Display g driver
subtitle: Logical gates to 7-segment display g driver
date: 2021-10-02T18:00:10.671Z
summary: "Just g segment of the 7-segment display"
draft: false
featured: false
tags:
  - digital systems
  - logical gates
  - encoders and decoders
image:
  filename: featured.png
  focal_point: Smart
  preview_only: false
  caption: Image 7-segment display make on EveryCircuit by me
---

TOCCI, Ronald J; WIDMER, Neal S.; MOSS, Gregory. Sistemas Digitais Princípios e Aplicações. 12. ed. Pearson. 2018
Capítulo 9  Circuitos MSI
9.12 (página 745)

<a href="https://everycircuit.com/circuit/6060338426150912">Digital Systems - 9.12 Just g segment of the 7-segment display - EveryCircuit</a><br>
<iframe width="560" height="360" src="https://everycircuit.com/embed/6060338426150912" frameborder="0"></iframe>