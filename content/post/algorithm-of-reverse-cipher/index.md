---
title: Algorithm of Reverse Cipher
subtitle: Reverse Cipher uses a pattern of reversing the string of plaintext to
  convert as ciphertext
date: 2020-06-24T20:09:02.329Z
summary: The algorithm of the reverse cipher.
draft: false
featured: false
authors:
  - admin
tags:
  - python
  - reverse cipher
  - cryptography
image:
  filename: featured.png
  focal_point: center
  preview_only: true
  caption: Photo by [Chris Ried](https://unsplash.com/@cdr6934) on
    [Unsplash](https://unsplash.com)
---
The algorithm of reverse cipher holds the following features:

* Reverse Cipher uses a pattern of reversing the string of plaintext to convert as ciphertext;
* The process of encryption and decryption is the same;
* To decrypt the ciphertext, we need to reverse the ciphertext to get the plaintext.

### Drawback

The major drawback of a reverse cipher is that it is very weak.

```python
message = 'This is a program to explain reverse cipher'
translated = '' # ciphertext is stored in this variable

i = len(message) - 1

while i >= 0:
    translated = translated + message[i]
    i = i - 1

print("The plaintext is.: {}".format(message.lower()))
print("The ciphertext is: {}".format(translated.upper()))
````

### Output

```bash
The plaintext is.: this is a program to explain reverse cipher
The ciphertext is: REHPIC ESREVER NIALPXE OT MARGORP A SI SIHT
```

### Explanation

The plaintext is stored in the variable message and the translated variable is used to store the ciphertext created.

The length of the plaintext is calculated using for loop and with help of index number. The characters are stored in the ciphertext variable translated which is printed in the last line.

Adapted by: [tutorialspoint](https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_reverse_cipher.htm)
