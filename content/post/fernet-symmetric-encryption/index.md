---
title: Fernet (symmetric encryption)
subtitle: Fernet guarantees that a message encrypted using it cannot be
  manipulated or read without the key.
date: 2020-06-24T23:30:17.888Z
summary: Fernet guarantees that a message encrypted using it cannot be
  manipulated or read without the key
draft: false
featured: false
authors:
  - admin
tags:
  - python
  - fernet
  - cryptography
  - symmetric encryption
image:
  filename: featured.png
  focal_point: Smart
  preview_only: false
---
Fernet guarantees that a message encrypted using it cannot be manipulated or read without the key. Fernet is an implementation of symmetric (also known as 'secret key') authenticated cryptography. Fernet also has support for implementing key rotation via MultiFernet.

```python
!pip install cryptography
```

### Code

```python
from cryptography.fernet import Fernet

my_secret = b'my deep dark secret'

key = Fernet.generate_key()
cipher_suite = Fernet(key)

cipher_text = cipher_suite.encrypt(my_secret)
plain_text = cipher_suite.decrypt(cipher_text)

print("The ciphertext is: {}".format(cipher_text))
print("The plaintext is.: {}".format(plain_text))
```

### Output

```bash
The ciphertext is: b'gAAAAABe8-LW_HKdLKQ3RtNpjLFFzo02TnyaVACgSuO56JoE06onY1avgxJsaaQmE40hLJC28m_NNZF9gtBQis_eTycN7rUe_G57VvDFLwC_83N3uR4ROBE='
The plaintext is.: b'my deep dark secret'
```

Adapted by: [tutorialspoint](https://www.tutorialspoint.com/cryptography_with_python/cryptography_with_python_modules_of_cryptography.htm)
