---
# Course title, summary, and position.
linktitle: Computação Forense
summary: Ciência da Computação
weight: 2

# Page metadata.
title: Computação Forense
date: "2020-01-09T00:00:00Z"
lastmod: "2020-01-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  example:
    name: Computação Forense
    weight: 2
---

### Plano de ensino

[ [PDF](files/uag00045.pdf) ]

### Canais para comunicação

- Classroom: [Clique aqui](https://classroom.google.com/)
- Professor:
  - Sérgio Francisco Tavares de Oliveira Mendonça [Telegram](https://t.me/sftom)

- Horários para atendimento:
  - Terças e sextas-feiras, 10h00 - 12h00, Prédio Docente 2, Térreo, Sala 12.
  - Consulte para outros horários.

### Materiais de apoio

- Material 1
- Material 2
- Material 3

### Atividades para realizar

- Atividade 1
- Atividade 2
- Atividade 3

### Verificação da aprendizagem

- Forma de entrega
  - Relatório das Atividades
  - Apresentação das Atividades

### Referências

- Material 1
- Material 2
- Material 3
