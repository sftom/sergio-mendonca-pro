---
title: 'Web semântica e educação: investigando a adequação de unidades de aprendizagem através de ontologias voltadas ao design educacional'
abstract: 'Estudos têm apontado a grande dificuldade envolvida na utilização de padrões voltados à interoperabilidade de Unidades de Aprendizagem utilizadas na abordagem de ensino a distância, mediados por computador, conectados à Internet. Vários organismos internacionais, como o IMS, IEEE e ADL, vêm apresentando interesse num trabalho colaborativo para a padronização de Educação a Distância, na tentativa de convergência da variedade de teorias e aplicações na área educacional. No que tange a Educação baseada em Web Semântica foi analisado o modelo da especificação IMS Learning Design. Um dos problemas apontados tem sido o desenvolvimento do Design Educacional adequados aos padrões de interoperabilidade e reutilização, independentes do ambiente de aprendizagem, através do uso de metadados que permitem a caracterização, gerenciamento, localiza¸c ao e correspondência entre sistemas. O presente trabalho possibilitou a imersão nestes dois grandes campos do saber, o Design Educacional e o Learning Design, que serviram de base para a investigação e caracterização desta pesquisa. Neste sentido, foi proposto um Módulo de Gestão e Adequação de Unidades de Aprendizagem baseado na Ontologia IMS Learning Design, construído a partir das observações e interações junto à equipe de Educação a Distância dos cursos de Educação Profissional do Estado de Pernambuco, partindo do objetivo de analisar as principais especificações utilizadas no desenvolvimento de Unidades de Aprendizagem, quanto aos aspectos de interoperabilidade, reutilização e adaptação de material educacional. Com ...'
authors:
- admin
date: 2010-01-31T00:00:00Z
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Attena Repositório Digital da UFPE](https://repositorio.ufpe.br)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by Attena Repositório Digital da UFPE
projects: []
publication: "*Attena Repositório Digital da UFPE, 2010*"
publication_short: '*Attena Repositório Digital da UFPE, 2010*'
publication_types:
- '7'
publishDate: 2010-01-31T00:00:00Z
slides: ''
summary: O presente trabalho possibilitou a imersão em dois grandes campos do saber, o Design Educacional e o Learning Design, e foi proposto um Módulo de Gestão e Adequação de Unidades de Aprendizagem baseado na Ontologia IMS Learning Design.
tags:
- Ontologies
- Semantic Web
- Education
- Unit of Learning
- IMS Learning Design
- Instructional Design
url_code: ''
url_dataset: ''
url_pdf: 'https://attena.ufpe.br/handle/123456789/2339'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
