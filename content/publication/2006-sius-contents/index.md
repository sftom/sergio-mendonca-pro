---
title: 'Implantação e desenvolvimento de ambiente gerenciador de conteúdo em escola da Rede Pública de Ensino com a finalidade de incentivar a produção colaborativa de literatura'
abstract: 'Desenvolvemos e implementamos uma ambiente gerenciador de conteúdos para o escolas, a fim de incentivar a produção colaborativa de textos e conteúdos didáticos.'
authors:
- admin
- Marcos Barros
date: 2006-04-06T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais SIUS, 2006](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais SIUS, 2006'
projects: []
publication: "*II Anais SIUS, 2006*"
publication_short: '*Anais SIUS, 2006*'
publication_types:
- '1'
publishDate: 2006-04-06T00:00:00Z
slides: ''
summary: 'Desenvolvemos e implementamos uma ambiente gerenciador de conteúdos para o escolas, a fim de incentivar a produção colaborativa de textos e conteúdos didáticos.'
tags:
- Learning Design
- Moodle
- Ambiente Virtual de Aprendizagem
- AVA
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
