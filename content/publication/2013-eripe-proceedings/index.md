---
title: 'Anais da III Escola Regional de Informática de Pernambuco, 2013'
abstract: 'Anais da III ERIPE é uma publicação anual da Escola Regional de Informática de Pernambuco, evento apoiado pela Sociedade Brasileira de Computação (SBC). Publica os trabalhos apresentados nas Escolas Regionais de Informática de Pernambuco, que constituem os trabalhos acadêmicos e são versões do estado atual da pesquisa desenvolvida em nível de Iniciação Científica, Trabalho de Conclusão de Curso, Mestrado e Doutorado, nos programas de Graduação e Pós-graduação nas áreas de Informática. O lançamento anual costuma acontecer durante a Escola Regional de Informática, ERIPE.'
authors:
- admin
date: 2013-11-13T00:00:00Z
isbn: ''
issn: '2317-5346'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIPE, 2013](http://www.journals.ufrpe.br/index.php/eripe/index)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIPE, 2013'
projects: []
publication: "*Anais da III Escola Regional de Informática de Pernambuco, 2013*"
publication_short: '*Anais da III ERIPE, 2013*'
publication_types:
- '1'
publishDate: 2013-11-13T00:00:00Z
slides: ''
summary: 'Anais da III ERIPE é uma publicação anual da Escola Regional de Informática de Pernambuco, evento apoiado pela Sociedade Brasileira de Computação (SBC).'
tags:
- Research
- Proceedings
- Anais
url_code: ''
url_dataset: ''
url_pdf: 'http://www.journals.ufrpe.br/index.php/eripe/issue/viewIssue/43/11'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
