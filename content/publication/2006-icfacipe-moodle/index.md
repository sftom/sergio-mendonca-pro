---
title: 'Rompendo Barreiras no Ensino Presencial: O Uso do Ambiente Moodle Estreitando a Interação Professor-Aluno'
abstract: 'Propomos atividades para validação de unidades de aprendizagem baseadas na especificação IMS LD, para identificar formas de interação (conteúdos, vídeos, grau de satisfação dos estudantes) entre professores e estudantes.'
authors:
- Flávia Araújo
- admin
- Marcos Barros
date: 2006-10-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais II IC-FACIPE, 2006](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais II IC-FACIPE, 2006'
projects: []
publication: "*II Anais II Encontro de de Iniciação Científica da FACIPE, 2006*"
publication_short: '*Anais II IC-FACIPE, 2006*'
publication_types:
- '1'
publishDate: 2006-10-10T00:00:00Z
slides: ''
summary: 'Propomos atividades para validação de unidades de aprendizagem baseadas na especificação IMS LD, para identificar formas de interação (conteúdos, vídeos, grau de satisfação dos estudantes) entre professores e estudantes.'
tags:
- Learning Design
- Moodle
- Ambiente Virtual de Aprendizagem
- AVA
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
