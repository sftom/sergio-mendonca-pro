---
title: 'Inserção da mulher na Ciência da Computação no município de Garanhuns'
abstract: 'O número de mulheres na Computação tem se mostrado cada vez menor. Na Universidade, em especial na Universidade Federal Rural de Pernambuco, em sua Unidade Acadêmica de Garanhuns, este número tem se apresentado de forma muito tímida. Em uma turma ingressante de um total de 40 estudantes, menos de 15% são mulheres. Claudia Maria Bauzer Medeiros, presidente da Sociedade Brasileira de Computação (SBC), informa que em 2005, no Brasil, dos estudantes de pós-graduação em Ciência da Computação, somente 25% eram mulheres, e entre docentes, de 25 a 30%. Em Ciência da Computação, há quatro ou cinco mulheres em turmas de 50 alunos. Espera-se como resultado, além do aumento do número de mulheres no curso de Ciência da Computação, uma maior participação de cada estudante do ensino médio, como embaixadora do curso de Ciência da Computação da UFRPE/UAG, para que estas estudantes atuem em suas escolas e comunidades como agentes de divulgação e assim, favorecer a inclusão da mulher na Ciência.'
authors:
- joao-silva-junior
- admin
date: 2013-11-06T00:00:00Z
isbn: ''
issn: '2317-5346'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIPE, 2013](http://www.journals.ufrpe.br/index.php/eripe/index)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIPE, 2013'
projects: []
publication: "*Anais da III Escola Regional de Informática de Pernambuco, 2013*"
publication_short: '*Anais da III ERIPE, 2013*'
publication_types:
- '1'
publishDate: 2013-11-06T00:00:00Z
slides: ''
summary: 'Espera-se como resultado, além do aumento do número de mulheres no curso de Ciência da Computação, uma maior participação de cada estudante do ensino médio, como  embaixadora do curso de Ciência da Computação da UFRPE/UAG, para que estas estudantes atuem em suas escolas e comunidades como agentes de divulgação e assim, favorecer a inclusão da mulher na Ciência.'
tags:
- Woman in Computer Science
- Inclusion
- Female Scientist
url_code: ''
url_dataset: ''
url_pdf: 'http://www.journals.ufrpe.br/index.php/eripe/article/download/373/307'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
