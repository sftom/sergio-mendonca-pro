---
title: 'Perdas em Sistemas de Distribuição de Água: estudo baseado em Revisão Sistemática'
abstract: No Brasil os indicadores de desempenho do setor de saneamento mostram uma alta ineficiência, apesar de nos últimos 10 anos ter sido observada uma redução de 6% no total das perdas, cerca de 40% do total de água produzida ainda é desperdiçada, quando, segundo o Banco Mundial, o valor aceitável para os países em desenvolvimento está entre 4% e 8%. Outro ponto a ser observado diz respeito a qualidade das informações disponíveis na grande rede mundial de computadores quando o assunto é ``perdas em sistemas de distribuição de água'', com informações dispersas ou apenas sobre ações gerenciais pontuais no monitoramento ou na apresentação de dados históricos sobre as perdas. Problema recorrente de relevância mundial, as perdas são responsáveis por substanciais prejuízos financeiros, sociais e ambientais, tanto às empresas quanto à sociedade, além de inferir em questão ambiental quando forçam à exaustão os mananciais de água no intuito de suprir a demanda de consumo. O objetivo desta pesquisa foi identificar a produção científica e literatura cinza disponível a fim de encontrar quais são os métodos, técnicas ou tecnologias utilizadas para detectar e mitigar os efeitos das perdas em sistemas de distribuição de água. O trabalho foi executado em duas etapas. Na primeira, realizou-se uma revisão de literatura e, posteriormente, um mapeamento sistemático da literatura a fim de identificar evidências publicadas na área de estudo. Dos estudos realizados podemos destacar uma importante contribuição, em função de perdas em sistemas de distribuição de água, identificando como são detectadas, quais são estratégias utilizadas para combater as perdas, e quais são as ferramentas ou métodos que auxiliam durante este processo.
authors:
- joao-silva-junior
- Gleibson Oliveira
- admin
- Fernanda Alencar
date: 2017-10-06T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [ABES/FENASAN 2017](http://abes-dn.org.br/?p=8805)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by ABES/FENASAN 2017
projects: []
publication: "*Proceedings ABES/FENASAN 2017*"
publication_short: '*Proceedings ABES/FENASAN 2017*'
publication_types:
- '1'
publishDate: 2017-12-12T00:00:00Z
slides: ''
summary: 'O nosso objetivo foi identificar a produção científica e literatura cinza disponível a fim de encontrar quais são os métodos, técnicas ou tecnologias utilizadas para detectar e mitigar os efeitos das perdas em sistemas de distribuição de água.'
tags:
- Saneamento
- Indicador de desempenho
- Perdas de água
- Sistema de distribuição de água
url_code: ''
url_dataset: ''
url_pdf: 'http://abes.locaweb.com.br/XP/XP-EasyArtigos/Site/Uploads/Evento36/TrabalhosCompletosPDF/XI-081.pdf'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
