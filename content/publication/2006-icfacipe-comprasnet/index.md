---
title: 'Administração Pública: Implantação e Gerenciamento de Cotações Eletrônicas no Hospital das Clínicas da Universidade Federal de Pernambuco'
abstract: 'Propomos a implantação e o gerenciamento de cotações eletrônicas, através do comprasnet.gov.br, no Hospital das Clínicas da Universidade Federal de Pernambuco.'
authors:
- admin
- Gilmar Silva
date: 2006-10-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais II IC-FACIPE, 2006](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais II IC-FACIPE, 2006'
projects: []
publication: "*II Anais II Encontro de de Iniciação Científica da FACIPE, 2006*"
publication_short: '*Anais II IC-FACIPE, 2006*'
publication_types:
- '1'
publishDate: 2006-10-10T00:00:00Z
slides: ''
summary: 'Propomos a implantação e o gerenciamento de cotações eletrônicas, através do comprasnet.gov.br, no Hospital das Clínicas da Universidade Federal de Pernambuco.'
tags:
- Public Health
- comprasnet.gov.br
- Budget Government
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
