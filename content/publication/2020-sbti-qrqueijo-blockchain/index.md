---
title: 'QRQueijo: sistema de emissão e validação de identificadores de queijos utilizando Blockchain'
abstract: 'This work aimed to develop an application that can generate identifiers for cheese and through a Blockchain network to validate such information. For this, a study was carried out on the functioning of a Blockchain and its characteristics of security, immutability, and scalability. The possibility of using Blockchain in the traceability of food products was identified. With this, the modeling and implementation of a system for recording data about cheeses and their manufacturers in a Blockchain were carried out. Such registration allows us to validate the information and make it available to consumers through the QRQuejio mobile application and to verify the authenticity of the data, through reading in the system database and the Blockchain, thus confirming the authenticity of the records.'
authors:
- anderson-morais
- ademario-silva
- admin
date: 2020-12-07T00:00:00Z
doi: '10.51359/1679-1827.2020.249166'
featured: false
image:
  placement: 1
  caption: 'Image by [Sociedade Brasileira de Tecnologia da Informação](https://sergio.mendonca.pro/)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by Sociedade Brasileira de Tecnologia da Informação
projects: [blockchain]
publication: "*Sociedade Brasileira de Tecnologia da Informação, 2020*"
publication_short: '*Sociedade Brasileira de Tecnologia da Informação, 2020*'
publication_types:
- '1'
publishDate: 2020-12-07T00:00:00Z
slides: ''
summary: 
tags:
- Blockchain
- Security
- Privacy
- Authenticity
- Data integrity
url_code: ''
url_dataset: ''
url_pdf: 'https://periodicos.ufpe.br/revistas/gestaoorg/article/download/249166/37850'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
