---
title: 'GeraQuest: um sistema para pesquisas de opinião através de dispositivos móveis: uma proposta para melhorar a acuracidade da coleta e do processamento de dados'
abstract: 'Este artigo relata o resultado da análise sobre o método convencional de pesquisa de opinião, realizado por meio de formulários em papel, mostrando as desvantagens em relação a sua logística de desenvolvimento. Como propósito geral buscou-se criar uma plataforma para o desenvolvimento e aplicação de pesquisas. A fim de estruturar o trabalho, mostraremos as seguintes metas secundarias: entender o problema, estudar técnicas de IHC, escolher ferramentas de desenvolvimento e manter iterações de criação e avaliação de protótipos. Ao final deste trabalho espera-se desenvolver uma plataforma com o máximo de experiência de usuário, que seja robusta, minimize custos e acelere todo o contexto de pesquisa.'
authors:
- joao-silva-junior
- admin
date: 2013-04-19T00:00:00Z
isbn: '978-85-7669-282-9'
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIN, 2013](https://www.academia.edu/7881092/III_ESCOLA_REGIONAL_DE_INFORM%C3%81TICA_REGIONAL_NORTE_AMAZONAS_E_RORAIMA)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIN, 2013'
projects: []
publication: "*Anais da III Escola Regional de Inform\'{a}tica -- Regional Norte 1 -- Amazonas e Roraima, 2013*"
publication_short: '*Anais da III ERIN, 2013*'
publication_types:
- '1'
publishDate: 2013-04-19T00:00:00Z
slides: ''
summary: 'Criamos uma plataforma para o desenvolvimento e aplicação de pesquisas. A fim de estruturar o trabalho, mostraremos as seguintes metas secundarias: entender o problema, estudar técnicas de IHC, escolher ferramentas de desenvolvimento e manter iterações de criação e avaliação de protótipos.'
tags:
- Opinion Survey
- IHC
- Mobile Device
url_code: ''
url_dataset: ''
url_pdf: 'https://www.academia.edu/7881092/III_ESCOLA_REGIONAL_DE_INFORM%C3%81TICA_REGIONAL_NORTE_AMAZONAS_E_RORAIMA'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
