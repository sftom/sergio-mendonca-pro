---
title: 'Investigação de uma Arquitetura para Gerenciamento de Documentos'
abstract: 'Um dos grandes desafios no gerenciamento de documentos consiste na identificação adequada, através da especificação e extração da informação, através de diversas abordagens e características. Definir e implementar técnicas para aquisição, pré-processamento, extração de características e reconhecimento de imagens de documentos. Comparar as técnicas implementadas e, dessa forma verificar a adequação das mesmas no ambiente proposto. O resultado prático, desse trabalho, servirá de protótipo para sistemas de análise de imagens de documentos.'
authors:
- admin
- Edson Carvalho Junior
- joao-silva-junior
date: 2013-11-06T00:00:00Z
isbn: ''
issn: '2317-5346'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIPE, 2013](http://www.journals.ufrpe.br/index.php/eripe/index)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIPE, 2013'
projects: []
publication: "*Anais da III Escola Regional de Informática de Pernambuco, 2013*"
publication_short: '*Anais da III ERIPE, 2013*'
publication_types:
- '1'
publishDate: 2013-11-06T00:00:00Z
slides: ''
summary: Investigar a definição e implementação de técnicas para aquisição, pré-processamento, extração de características e reconhecimento de imagens em documentos.
tags:
- Data Extraction
- Data Management
- Image Recognition
url_code: ''
url_dataset: ''
url_pdf: 'http://www.journals.ufrpe.br/index.php/eripe/article/download/363/297'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
