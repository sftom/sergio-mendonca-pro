---
title: 'Usando o Ambiente Virtual de Aprendizagem Moodle para professores e alunos'
abstract: 'Propomos a utilização da plataforma Moodle na autoria e validação de unidades de aprendizagem baseadas na especificação IMS LD, baseado no conceito de blended learning, estendendo os estilos de interação possíveis entre estudantes-professores e estudantes-estudantes e possibilitando a incorporação de novos estilos de interação.'
authors:
- admin
date: 2006-05-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais EINIPTEC, 2006](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais EINIPTEC, 2006'
projects: []
publication: "*II Anais EINIPTEC, 2006*"
publication_short: '*Anais EINIPTEC, 2006*'
publication_types:
- '1'
publishDate: 2006-05-10T00:00:00Z
slides: ''
summary: 'Propomos a utilização da plataforma Moodle na autoria e validação de unidades de aprendizagem baseadas na especificação IMS LD, baseado no conceito de blended learning, estendendo os estilos de interação possíveis entre estudantes-professores e estudantes-estudantes e possibilitando a incorporação de novos estilos de interação.'
tags:
- Learning Design
- Moodle
- Ambiente Virtual de Aprendizagem
- AVA
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
