---
title: Emissão segura de diplomas digitais através do registro em Blockchain
abstract: With the advancement of technologies and the increase for information generated, new needs for systems security arise. This article proposes the use of Blockchain to give a higher level of security to the process of issuing digital degrees by higher education institutions. A bibliographic search was carried out to understand the security features of the Blockchain and the level of confidence it presents, one should know the process of issuing degrees by universities, and what security procedures are used. Then, it results in a web application and a Blockchain simulation, to demonstrate the issuance of a digital diploma and its registration. The system also allows the validation of the diplomas issued, performing a consultation at several levels, checking if the document is registered in the system's database and in the Blockchain, in order to prove its reliability.
authors:
- anderson-morais
- admin
- Fernando Dias
date: 2020-12-07T00:00:00Z
doi: '10.51359/2317-0115.2020.249167'
featured: false
image:
  placement: 1
  caption: 'Image by [Sociedade Brasileira de Tecnologia da Informação](https://sergio.mendonca.pro/)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by Sociedade Brasileira de Tecnologia da Informação
projects: [blockchain]
publication: "*Sociedade Brasileira de Tecnologia da Informação, 2020*"
publication_short: '*Sociedade Brasileira de Tecnologia da Informação, 2020*'
publication_types:
- '1'
publishDate: 2020-12-07T00:00:00Z
slides: ''
summary: .
tags:
- Blockchain
- Security
- Privacy
url_code: ''
url_dataset: ''
url_pdf: 'https://periodicos.ufpe.br/revistas/RMP/article/view/249167/38280'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
