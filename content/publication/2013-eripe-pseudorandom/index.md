---
title: 'Simulação: Pseudoaleatoriedade, um estudo sobre o método do meio do quadrado'
abstract: 'Área da matemática estreitamente relacionada com os métodos computacionais, a geração de números pseudoaleatórios é bastante discutida e tem aplicação em várias situações como por exemplo na análise de algoritmos e na criptografia. Neste resumo é discutido o método dos meios quadrados para a geração de números pseudoaleatórios'
authors:
- joao-silva-junior
- admin
date: 2013-11-06T00:00:00Z
isbn: ''
issn: '2317-5346'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIPE, 2013](http://www.journals.ufrpe.br/index.php/eripe/index)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIPE, 2013'
projects: []
publication: "*Anais da III Escola Regional de Informática de Pernambuco, 2013*"
publication_short: '*Anais da III ERIPE, 2013*'
publication_types:
- '1'
publishDate: 2013-11-06T00:00:00Z
slides: ''
summary: 'Os métodos computacionais, a geração de números pseudoaleatórios é bastante discutida e tem aplicação em várias situações como por exemplo na análise de algoritmos e na criptografia. Neste resumo é discutido o método dos meios quadrados para a geração de números pseudoaleatórios'
tags:
- Simulation
- Pseudorandom
- Half Square Method
- Computational Simulation
url_code: ''
url_dataset: ''
url_pdf: 'http://www.journals.ufrpe.br/index.php/eripe/article/download/372/306'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
