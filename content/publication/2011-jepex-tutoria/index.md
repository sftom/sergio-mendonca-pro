---
title: 'Tutoria: Estratégias para a redução dos índices de evasão e reprovação nos períodos iniciais do Bacharelado em Ciência da Computação'
abstract: 'O objetivo deste artigo é demonstrar o funcionamento do programa de tutoria, apoio aos discentes, implantado na Universidade Federal Rural de Pernambuco - Unidade Acadêmica de Garanhuns, UFRPE-UAG, utilizando dados dos componentes curriculares Linguagem de Programação e Laboratório de Informática do Bacharelado em Ciência da Computação. Uma observação importante é quanto ao índice de reprovação. No caso analisado das disciplinas Linguagem de Programação e Laboratório de Informática possuem respectivamente 17% e 16% como média de seus índices de reprovação. Tomando como exemplo a disciplina Laboratório de Informática, inicialmente ofertando quarenta vagas, no semestre seguinte necessitaria de mais sete vagas para suprir os reprovados do semestre anterior, e no seguinte de mais oito o que totaliza cinquenta e cinco vagas para uma disciplina que inicialmente ofertara apenas quarenta. Fica clara então a necessidade de reduzir as reprovações gerais.'
authors:
- joao-silva-junior
- admin
date: 2011-10-22T00:00:00Z
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [JEPEX 2011](http://www.eventosufrpe.com.br/2011/trabalhos.php)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by JEPEX 2011
projects: []
publication: "*JEPEX/UFRPE, 2011*"
publication_short: '*JEPEX/UFRPE, 2011*'
publication_types:
- '1'
publishDate: 2011-10-22T00:00:00Z
slides: ''
summary: é demonstrar o funcionamento do programa de tutoria, apoio aos discentes, implantado na Universidade Federal Rural de Pernambuco - Unidade Acadêmica de Garanhuns, UFRPE-UAG...
tags:
- Tutoria
- Evasão
- Reprovação
url_code: ''
url_dataset: ''
url_pdf: 'http://www.eventosufrpe.com.br/2011/trabalhos.php'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
