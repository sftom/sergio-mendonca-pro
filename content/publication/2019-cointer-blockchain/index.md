---
title: 'A Tecnologia Blockchain como Ferramenta de Segurança da Informação na Produção de Queijos: um relato de caso sobre a formação profissional'
abstract: 'Relato de Experiência no IV Congresso Internacional das Ciências Agrárias - IV COINTER PDVAgro 2019. Apresentado por Mateus Costa.'
authors:
- mateus-costa
- admin
date: 2019-12-05T00:00:00Z
issn: '2526-7701'
doi: '10.31692/2526-7701'
featured: false
image:
  placement: 1
  caption: 'Image by [COINTER/PDVAgro 2019](https://cointer.institutoidv.org/pdvagro/pdvagro2019.php)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by COINTER/PDVAgro 2019
projects: [blockchain]
publication: "*Proceedings COINTER/PDVAgro, 2019*"
publication_short: '*Proceedings COINTER/PDVAgro, 2019*'
publication_types:
- '1'
publishDate: 2019-12-05T00:00:00Z
slides: ''
summary: ''
tags:
- Blockchain
- Engenharia de Alimentos
- Produção de Queijos
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
