---
title: 'Atividades escolares em redes sociais: estabelecendo relações colaborativas para a construção do conhecimento na Internet'
abstract: 'Capítulo de livro, publicado na coletânea CTSA: Experiências Multi e Interdisciplinares no Ensino de Ciências e Matemática, organizado pela Prof. Marly Oliveira, PhD.'
authors:
- admin
date: 2009-07-08T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Academia de Projetos, 2009](http://www.academiadeprojetos.com.br/p/publicacoes.html)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Academia de Projetos, 2009'
projects: []
publication: "*CTSA: Experiências Multi e Interdisciplinares no Ensino de Ciências e Matemática, 2009*"
publication_short: '*CTSA: Experiências Multi e Interdisciplinares no Ensino de Ciências e Matemática, 2009*'
publication_types:
- '6'
publishDate: 2009-07-08T00:00:00Z
slides: ''
summary: 'Capítulo de livro, publicado na coletânea CTSA: Experiências Multi e Interdisciplinares no Ensino de Ciências e Matemática, organizado pela Prof. Marly Oliveira, PhD.'
tags: 
- Social Network
- Scholar
- Internet
url_code: ''
url_dataset: ''
url_pdf: 'http://www.academiadeprojetos.com.br/p/publicacoes.html'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
