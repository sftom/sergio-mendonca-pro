---
title: 'A Local Water Control Architecture based on Internet of Things to Water Supply Crisis'
abstract: Water is one of the most valuable assets for humanity, it represents life, but its misuse, misapplication and inappropriate distribution can result in considerable losses, escalating to major problems. The objective of this study is to present a solution based on the Internet of Things, for domestic monitoring and control of water distribution, to prevent waste. We adopted an IoT-Ontology approach to ensure semantic (solid representations) between entities and other devices. The results presented in this study have been achieved by initially considering a low cost and low power consumption device. Further implementations of the solution presented will consider improvements of the experimental testing for end users and device development with government funding agencies.
authors:
- admin
- joao-silva-junior
- Fábio Bonifacio
- Fernanda Alencar
date: 2016-05-14T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [SIM/EMicro/ASYNC-2016](https://www.inf.ufrgs.br/gme/emicro/sime.html)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by SIM/EMicro/ASYNC-2016'
projects: [blockchain]
publication: "*Proceedings 31st South Symposium on Microelectronics, 2016*"
publication_short: '*Proceedings SIM, 2016*'
publication_types:
- '1'
publishDate: 2016-05-14T00:00:00Z
slides: ''
summary: We aimed to develop and combine an ontology and internet of things models and built a low cost solution.
tags:
- Local Water Control
- WLC
- Embedded Systems
- Internet of Things
- IoT
url_code: ''
url_dataset: ''
url_pdf: 'https://inf.ufrgs.br/gme/emicro'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
