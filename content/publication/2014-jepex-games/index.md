---
title: 'Imersão Tecnológica em Desenvolvimento de Jogos Digitais'
abstract: 'O crescimento de aplicações e dispositivos tecnológicos vem aumentando significativamente, o que tem possibilitado a interatividade entre pessoas e grupo de pessoas e ao mesmo tempo tem a função de entretenimento. Nesse contexto, estão inclusos os jogos digitais, que com o passar do tempo, novas técnicas, mecânicas, softwares e ferramentas vem sendo disponibilizadas no mercado, aumentando o leque de opções com distintas finalidades.   Neste trabalho, temos como objetivos o desenvolvimento de Jogos Digitais a partir da apresentação de conceitos, mecânicas e processo de desenvolvimento que tornem possível aos integrantes uma melhor compreensão do problema de forma a facilitar o processo desenvolvimento.'
authors:
- thiago-oliveira
- Kádna Camboim
- admin
date: 2014-11-25T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da XIV JEPEX, 2014](http://www.eventosufrpe.com.br/2014/programacao_uag.asp)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da XIV JEPEX, 2014'
projects: []
publication: "*Anais da XIV Jornada de Ensino, Pesquisa e Extensão*"
publication_short: '*Anais da XIV JEPEX, 2014*'
publication_types:
- '1'
publishDate: 2014-11-25T00:00:00Z
slides: ''
summary: 'Os jogos apresentam, além da interatividade entre pessoas e grupos de pessoas, um grande crescimento entre os principais meios de entretenimento da atualidade. O objetivo central é estudar e aplicar técnicas, mecânicas, software e ferramentas para desenvolvimento de jogos digitais por estudantes secundaristas e superior na comunidade de Garanhuns e cidades circunvizinhas.'
tags:
- Immersive Game Design
- Game Framework
- Game Technologies
url_code: ''
url_dataset: ''
url_pdf: 'http://www.eventosufrpe.com.br/2014/programacao_uag.asp'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
