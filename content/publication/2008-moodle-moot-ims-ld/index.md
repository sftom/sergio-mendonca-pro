---
title: 'A Utilização da Ontologia IMS Learning Design na Plataforma Moodle'
abstract: 'O principal mercado a ser atingido é o de educação corporativa (entidades do poder público e privado) e acadêmica (instituições de ensino superior, tecnológico ou médio). Propomos a utilização da ontologia IMS Learning Design, pela plataforma Moodle na autoria e validação de unidades de aprendizagem baseadas na especificação IMS LD, baseado no conceito de blended learning, estendendo os estilos de interação possíveis entre os usuários e possibilitando a incorporação de novos estilos de interação.'
authors:
- admin
- Flávia Araújo
date: 2008-11-29T00:00:00Z
isbn: ''
issn: '1982-1611'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais II Moodle Moot, 2008](https://sergio.mendonca.pro/files/2008-anais-moodle-moot.pdf){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais II Moodle Moot, 2008'
projects: []
publication: "*II Anais II Moodle Moot, 2008*"
publication_short: '*Anais II Moodle Moot, 2008*'
publication_types:
- '1'
publishDate: 2008-11-30T00:00:00Z
slides: ''
summary: 'Propomos a utilização da ontologia IMS Learning Design, pela plataforma Moodle na autoria e validação de unidades de aprendizagem baseadas na especificação IMS LD, baseado no conceito de blended learning, estendendo os estilos de interação possíveis entre os usuários e possibilitando a incorporação de novos estilos de interação.'
tags: 
- IMS-LD
- Learning Design
- Moodle
- Ambiente Virtual de Aprendizagem
- AVA
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
