---
title: 'AppDay ERIPE 2013'
abstract: 'O objetivo deste trabalho é apresentar o AppDay, uma iniciativa da Microsoft do Brasil para promover o desenvolvimento de aplicações e as novas tecnologias embarcadas no Microsoft Windows 8. Formatado como um minicurso de um dia, o AppDay será ministrado por João Ferreira e Severino Neto.'
authors:
- joao-silva-junior
- severino-costa-neto
- admin
date: 2013-11-06T00:00:00Z
isbn: ''
issn: '2317-5346'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da III ERIPE, 2013](http://www.journals.ufrpe.br/index.php/eripe/index)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da III ERIPE, 2013'
projects: []
publication: "*Anais da III Escola Regional de Informática de Pernambuco, 2013*"
publication_short: '*Anais da III ERIPE, 2013*'
publication_types:
- '1'
publishDate: 2013-11-06T00:00:00Z
slides: ''
summary: 'O objetivo foi apresentar o AppDay, uma iniciativa da Microsoft do Brasil para promover o desenvolvimento de aplicações e as novas tecnologias embarcadas no Microsoft Windows 8.'
tags:
- AppDay
- Microsoft
url_code: ''
url_dataset: ''
url_pdf: 'http://www.journals.ufrpe.br/index.php/eripe/article/download/385/319'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
