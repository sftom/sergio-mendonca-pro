---
title: 'Simulador do Processo de Instalação do Rocks Clusters com uso de Técnicas de IHC'
abstract: 'Disciplinas sobre programação paralela necessitam otimizar o processo de configuação das estruturas de cluster que utilizam, por não ser esse seu foco primário. Para auxiliar na resolução desse problema é apresentado neste trabalho um simulador do processo de instalação do Rocks Clusters. O simulador utiliza princípios de Interação Humano-Computador para impedir que erros sejam cometidos por estudantes durante o processo.'
authors:
- David Beserra
- Anderson Cavalcanti
- Luciano Souza
- Rubens Karman
- Jean Araujo
- admin
date: 2014-10-17T00:00:00Z
isbn: ''
issn: '2179-1953'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da V ERI-MT, 2014](https://www.academia.edu/22377917/Anais_da_V_Escola_Regional_de_Inform%C3%A1tica_de_Mato_Grosso_-_2014)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da V ERI-MT, 2014'
projects: []
publication: "*Anais da V Escola Regional de Informática de Mato Grosso, 2014*"
publication_short: '*Anais da V ERI-MT, 2014*'
publication_types:
- '1'
publishDate: 2014-10-17T00:00:00Z
slides: ''
summary: 'Neste trabalho construímos um simulador do processo de instalação do Rocks Clusters. O simulador utiliza princípios de Interação Humano-Computador para impedir que erros sejam cometidos por estudantes durante o processo.'
tags:
- Simulation
- IHC
- Parallel Programming
- Computational Simulation
url_code: ''
url_dataset: ''
url_pdf: 'https://www.academia.edu/22377917/Anais_da_V_Escola_Regional_de_Inform%C3%A1tica_de_Mato_Grosso_-_2014'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
