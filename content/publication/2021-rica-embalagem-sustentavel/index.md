---
title: 'Embalagem sustentável de alimentos: uma revisão sistemática'
abstract: 'As embalagens desempenham um papel muito importante na conservação e comercialização dos alimentos, entretanto, suas contribuições foram deixadas de lado em muitas discussões de sustentabilidade. Por muito tempo, foram tidas como um impacto ambiental indesejável e considerava-se que sua redução ou eliminação eram as melhores soluções. Entretanto, pesquisas mais recentes as identificam como ferramentas importantes para o cumprimento dos objetivos de sustentabilidade da Agenda 2030, principalmente do objetivo 12, “assegurar padrões de consumo e produção sustentáveis”. Nesse contexto, o aumento da eficiência dos sistemas de embalagens de alimentos é imprescindível. Entretanto, as peculiaridades desses sistemas são várias e interagem de diferentes formas com os múltiplos atores e cenários do sistema alimentar global, o que dificulta a adoção de medidas concretas em direção ao cumprimento da agenda. Por isso, para melhor compreensão do conceito de embalagem sustentável de alimentos e do seu atual estado da arte, foi realizada uma revisão sistemática desse tópico. Com isso, foram analisados 40 trabalhos de 18 periódicos, produtos da colaboração de pesquisadores de 61 instituições de 19 países, que têm investigado o papel das embalagens nas cadeias de produção e distribuição de alimentos com diferentes metodologias. Os métodos mais adotados nessas publicações foram Revisão, Avaliação do Ciclo de Vida e Entrevistas, inclusive combinados em um mesmo trabalho, dada a complexidade do tema. Trata-se de um problema multidisciplinar que vem sendo abordado no meio científico há um certo tempo, mas que ainda contém lacunas importantes que demandam mais estudos. Os dados disponíveis ainda são limitados principalmente no sentido de quantificar e qualificar as perdas e desperdícios de alimentos associadas a embalagens inadequadas. Além disso, é preciso investigar as peculiaridades dos múltiplos sistemas embalagem-alimento, bem como a promoção do conhecimento relativo às embalagens entre consumidores no sentido de quebrar paradigmas já ultrapassados que focam no seu impacto individual. Dessa forma, foi possível reunir um corpo de conhecimento atualizado e, para além disso, um conjunto de recomendações que podem guiar pesquisas futuras.'
authors:
- Mariana Costa
- Desirré Bernardo
- Anny Vidal
- admin
- Glêce Milene Gomes
- Romero Sales Filho
date: 2021-10-28T00:00:00Z
doi: '10.6008/CBPC2179-6858.2021.010.0033'
featured: false
image:
  placement: 1
  caption: 'Image by [RICA](https://sergio.mendonca.pro/)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by RICA
projects: [blockchain]
publication: "*RICA, 2021*"
publication_short: '*RICA, 2021*'
publication_types:
- '1'
publishDate: 2021-10-28T00:00:00Z
slides: ''
summary: 
tags:
- Blockchain
- Security
- Privacy
- Authenticity
- Data integrity
url_code: ''
url_dataset: ''
url_pdf: 'https://sustenere.co/index.php/rica/article/view/6353'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
