---
title: 'A Blockchain-based Ontology for the Internet of Things Security'
abstract: 'Recent studies have revealed serious security breaches in the Internet of Things (IoT) devices. Today’s architecture does not guarantee an adequate level of security, so attacks on data authenticity and integrity are among the top concerns when dealing with IoT-based environments. In this context, the objective of this work was to develop an ontology model for Blockchainbased IoT (BIoT) that ensures an adequate level of security. We implemented an ontology-based middleware that represents semantic knowledge. BIoT is independent of application context and protects against reported attacks from the fundamentals of blockchain networks. Initially, we built, through the hypothetical-deductive method, a BIoT model based on particular domain ontologies. We then interact between IoT devices and security ontologies and blockchain network concepts to capture characteristics. We then performed performance tests (sandbox); bench testing with Zigbee devices (testbed); knowledge base assessment; and research with experts through a questionnaire and semi-structured interviews to evaluate the proposal. We still adopt security criteria against possible known attacks in the literature. Thus, the ontology provided insight into security properties to monitor vulnerabilities in the IoT ecosystem and blockchain network structure, thereby ensuring data integrity, confidentiality, and privacy. Through the collected information, the BIoT model was built that presented the following advantages: adequate time processing; decentralized architecture, less susceptible to attack; presence of a stable network, even with the increase in the number of nodes, and consequently the packet traffic; possibility of improving the efficiency of data integrity verification; and increased availability of processing and memory resources for specific need environments. Thus, the model can be considered a promising alternative.'
authors:
- admin
date: 2019-08-30T00:00:00Z
doi: ''
featured: true
image:
  placement: 1
  caption: 'Image by [Attena Repositório Digital da UFPE](https://repositorio.ufpe.br)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by Attena Repositório Digital da UFPE
projects: [blockchain]
publication: "*Attena Repositório Digital da UFPE, 2019*"
publication_short: '*Attena Repositório Digital da UFPE, 2019*'
publication_types:
- '7'
publishDate: 2019-08-30T00:00:00Z
slides: ''
summary: We built, through the hypothetical-deductive method, a BIoT model based on particular domain ontologies. We then interact between IoT devices and security ontologies and blockchain network concepts to capture characteristics. We then performed performance tests (sandbox).
tags:
- Internet of Things
- Blockchain
- Security
- Privacy
- Authenticity
- Data integrity
- IoT
url_code: ''
url_dataset: ''
url_pdf: 'https://repositorio.ufpe.br/handle/123456789/35813'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
