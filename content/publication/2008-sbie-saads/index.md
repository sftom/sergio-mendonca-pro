---
title: 'SAADS (Sistema de avaliação do aprendizado em dinâmicas com simuladores): Uma proposta para melhorar o feedback dos participantes'
abstract: 'A constante evolução da sociedade e das tecnologias tem demonstrado um grande crescimento no campo empresarial. A aprendizagem organizacional e dinâmicas com simuladores têm se tornado essenciais para a redução dos erros nas tomadas de decisões empresariais. Neste trabalho, propomos o Sistema de Avaliação do Aprendizado em Dinâmicas com Simuladores – SAADS, software utilizado para auxiliar o processo de avaliação em ambientes de simulações empresariais, facilitando o feedback no processo de aprendizado dos participantes.'
authors:
- Alex Gomes
- Félix Farias
- Raoni Franco
- Guilherme Carvalho
- Daniel Penaforte
- Hector Paulo
- admin
- Flávia Souza
date: 2008-01-08T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [ResearchGate, 2008](https://researchgate.net)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by ResearchGate, 2008'
projects: []
publication: "*XIX Simpósio Brasileiro de Informática na Educação, 2008*"
publication_short: '*Proceedings SBIE, 2008*'
publication_types:
- '1'
publishDate: 2008-01-08T00:00:00Z
slides: ''
summary: 'A aprendizagem organizacional e dinâmicas com simuladores têm se tornado essenciais para a redução dos erros nas tomadas de decisões empresariais. Neste trabalho, propomos o Sistema de Avaliação do Aprendizado em Dinâmicas com Simuladores.'
tags: 
- Learning Simulation
- Evaluation
- Organizational Learning
url_code: ''
url_dataset: ''
url_pdf: 'https://researchgate.net'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
