---
title: 'A Incorporação da Teoria da Flexibilidade Cognitiva em Ambientes Virtuais de Aprendizagem para Incrementar a Aprendizagem em Sala de Aula Presencial'
abstract: 'Este trabalho investigou a aplicação da Teoria da Flexibilidade Cognitiva em Ambientes Virtuais de Ensino, a partir da adoção de estratégias de suporte à mediação da aprendizagem, proporcionando aos alunos aprender a aprender e aprender colaborativamente e incrementar a aprendizagem na sala de aula presencial.'
authors:
- Flávia Araújo
- Marcelo Leão
- admin
- Alex Gomes
date: 2008-03-08T00:00:00Z
isbn: ''
issn: '1984-1175'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais Simpósio Hipertexto, 2008](http://www.nehte.com.br/simposio/anais/simposio2008.html)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais Simpósio Hipertexto, 2008'
projects: []
publication: "*II Simpósio Hipertexto e Tecnologias na Educação, 2008*"
publication_short: '*Anais Simpósio Hipertexto, 2008*'
publication_types:
- '1'
publishDate: 2008-03-08T00:00:00Z
slides: ''
summary: 'Nós investigamos a aplicação da Teoria da Flexibildiade Cognitiva em Ambientes Virtuais de Ensino para incrementar a aprendizagem na sala de aula presencial.'
tags: 
- Cognitive Flexibility
- Virtual Environment
- Teaching Practice
- Teaching and Learning
url_code: ''
url_dataset: ''
url_pdf: 'https://bit.ly/3gddeG7'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
