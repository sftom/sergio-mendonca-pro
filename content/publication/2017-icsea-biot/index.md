---
title: 'The Blockchain-based Internet of Things development: Initiatives and challenges'
abstract: The Internet was originally built based on trust. After several leaks of
  information, new risks and challenges are introduced. In recent years, we have used
  even more new devices based on the Internet. Among the main concerns reported on
  the literature, we need some special attention to trust, protection of data and
  privacy. In this  scenario, a new paradigm has emerged, some information security
  based on transparency instead of current models of information security on closed
  and obscure approaches. Some initiatives have been emerging with Blockchain methods
  and technologies. In this paper, we propose to build an initial view of the model,
  as a result of our preliminary investigations, described in the Methodology as systematic
  mapping. The initial results allowed the perception of the initial requirements
  involved and open problems. We report on some frameworks, models, approaches, and
  other Blockchainbased Internet of Things (IoT) initiatives. We also evaluate the
  adherence of each paper to ten IoT key requirements. This work contributes to the
  new and still developing body of knowledge in the areas of security, privacy and
  trust. Our findings are useful not only for future studies in the Academy but also
  for companies from various sectors present in the Internet ecosystem. They can benefit
  from the consolidated knowledge and use it to guide the definition of their development
  processes geared to the new paradigms of the IoT.
authors:
- admin
- joao-silva-junior
- Fernanda Alencar
date: 2017-10-12T00:00:00Z
isbn: '978-1-61208-590-6'
issn: '2308-4235'
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [IARIA/ICSEA](https://www.iaria.org/conferences2017/ICSEA17.html)'
  focal_point: 'Center'
  preview_only: false
  alt_text: Image by IARIA/ICSEA 2017
projects: [blockchain]
publication: "*Proceedings ICSEA, 2017*"
publication_short: '*Proceedings ICSEA, 2017*'
publication_types:
- '1'
publishDate: 2017-12-12T00:00:00Z
slides: ''
summary: We aimed to develop and combine blockchain and internet of things models
  and compare with existing solution.
tags:
- Blockchain
- Internet of Things
- IoT
- Ontology
- Privacy
- Security
url_code: ''
url_dataset: ''
url_pdf: 'https://www.thinkmind.org/download.php?articleid=icsea_2017_2_20_10012'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
