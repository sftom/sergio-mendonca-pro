---
title: 'Web Semântica e Educação: das Unidades de Aprendizagem às Ontologias'
abstract: 'Estudos têm apontado a grande dificuldade envolvida na utilização de padrões voltados à interoperabilidade de Unidades de Aprendizagem utilizadas na abordagem de ensino a distância, mediados por computador, conectados à Internet. Vários organismos internacionais, como o IMS, IEEE e ADL, vêm apresentando interesse num trabalho colaborativo para a padronização de Educação a Distância, na tentativa de convergência da variedade de teorias e aplicações na área educacional. No que tange a Educação baseada em Web Semântica foi analisado o modelo da especificação IMS Learning Design. Um dos problemas apontados tem sido o desenvolvimento do Design Educacional adequados aos padrões de interoperabilidade e reutilização, independentes do ambiente de aprendizagem, através do uso de metadados que permitem a caracterização, gerenciamento, localização e correspondência entre sistemas. O presente trabalho possibilitou a imersão em diversos campos do saber, o Design Educacional, Ontologias... que serviram de base para a investigação e caracterização desta pesquisa. Neste sentido, foi proposto um Módulo de Gestão e Adequação de Unidades de Aprendizagem baseado na Ontologia IMS Learning Design.'
authors:
- admin
date: 2016-06-14T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Amazon, 2016](https://amzn.to/2Xhn15b)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Amazon, 2016'
projects: []
publication: "*Web Semântica e Educação: das Unidades de Aprendizagem às Ontologias, 2016*"
publication_short: '*Amazon, 2016*'
publication_types:
- '5'
publishDate: 2016-06-14T00:00:00Z
slides: ''
summary: 'Um dos problemas apontados tem sido o desenvolvimento do Design Educacional adequados aos padrões de interoperabilidade e reutilização, independentes do ambiente de aprendizagem, através do uso de metadados que permitem a caracterização, gerenciamento, localização e correspondência entre sistemas.'
tags: 
- Ontologies
- Semantic Web
- Education
- Unit of Learning
- IMS Learning Design
- Instructional Design
url_code: ''
url_dataset: ''
url_pdf: 'https://amzn.to/2Xhn15b'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
