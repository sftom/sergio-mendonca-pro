---
title: 'Aplicação de Problem-Based Learning (PBL) na Resolução de Problemas Matemáticos e Computacionais na Comunidade Escolar do Município de Garanhuns'
abstract: 'Com o crescimento do mercado de Tecnologia da Informação (TI) e seu papel na criação de vantagens nas organizações tem-se aumentado a demanda de profissionais da área. Porém, existe a necessidade de despertar nos alunos a conhecer e atuar na área de TI no sentido de aumentar a capacidade produtiva tecnológica na região. Este trabalho objetiva instigar alunos de ensino médio das escolas de Garanhuns a atuar nas áreas exatas, especificamente na computação. Este trabalho objetiva instigar alunos de ensino médio das escolas de Garanhuns a atuar nas áreas exatas, especificamente na computação. A construção tradicional do conhecimento realizada atualmente tem base em orientação cognitiva, onde um docente repassa teorias e práticas para o discente, este modelo tem como o docente o principal agente no círculo de aprendizagem, interagindo de maneira ativa, deixando assim, o discente como agente passivo. O modelo Problem-Based Learning (PBL) ou Aprendizagem Baseada em Problemas, permite docentes expor problemas reais para os discentes, deixando-os identificarem a problemática, investigarem, debaterem, interpretarem e produzirem algumas possíveis soluções ou recomendações'
authors:
- severino-costa-neto
- Kádna Camboim
- admin
date: 2014-11-25T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais da XIV JEPEX, 2014](http://www.eventosufrpe.com.br/2014/programacao_uag.asp)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais da XIV JEPEX, 2014'
projects: [pbl-in-computerscience]
publication: "*Anais da XIV Jornada de Ensino, Pesquisa e Extensão*"
publication_short: '*Anais da XIV JEPEX, 2014*'
publication_types:
- '1'
publishDate: 2014-11-25T00:00:00Z
slides: ''
summary: 'Este trabalho objetiva despertar o interesse nos alunos de ensino médio das escolas de Garanhuns a atuar nas áreas exatas, especificamente na computação. O modelo Problem-Based Learning (PBL) ou Aprendizagem Baseada em Problemas, permite docentes expor problemas reais para os discentes, deixando-os identificarem a problemática, investigarem, debaterem, interpretarem e produzirem algumas possíveis soluções ou recomendações.'
tags:
- Learning Strategies
- Problem-based Learning
- PBL
url_code: ''
url_dataset: ''
url_pdf: 'http://www.eventosufrpe.com.br/2014/programacao_uag.asp'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
