---
title: 'Desenvolvimento Distribuído de uma Aplicação de Telessaúde com a Metodologia Ágil SCRUM'
abstract: 'Este artigo apresenta um estudo de caso completo de Desenvolvimento Distribuído de Software, apoiado na metodologia ágil SCRUM. Este estudo de caso foi realizado através da constituição de uma Fábrica de Software para o desenvolvimento de uma Biblioteca Multimídia, um sistema para o compartilhamento de objetos de aprendizagem utilizados pelos projetos de telemedicina e telessaúde do Núcleo de Telesaúde da UFPE'
authors:
- Alexandre Luna
- Cleyverson Costa
- Raphael Patrício
- Ana Araújo
- Haglay Silva
- João Pessoa
- Raony Araújo
- Marcela Moraes
- Maria Andrade
- admin
- Fabrício Dias
- Marcello Mello
- Magdala Novaes
- Jones Albuquerque
- Sílvio Meira
date: 2008-11-29T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais CBIS, 2008](http://www.nehte.com.br/simposio/anais/simposio2008.html)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais CBIS, 2008'
projects: []
publication: "*XI Congresso Brasileiro de Informática em Saúde (CBIS), 2008*"
publication_short: '*Anais CBIS, 2008*'
publication_types:
- '1'
publishDate: 2008-11-29T00:00:00Z
slides: ''
summary: 'Nós desenvolvemos através das metodologias de DDS e Scrum uma biblioteca multimídia, um sistema para o compartilhamento de objetos de aprendizagem utilizados pelos projetos de telemedicina e telessaúde do Núcleo de Telesaúde da UFPE.'
tags: 
- Software Factory
- Distributed Development Software 
- Telehealth
- DDS
url_code: ''
url_dataset: ''
url_pdf: 'https://bit.ly/2TrFT02'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
