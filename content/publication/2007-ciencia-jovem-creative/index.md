---
title: 'Lixo e/ou matéria-prima: criatividade e responsabilidade sustentável'
abstract: 'Propomos algumas práticas com resíduos oriundos da escola, promovendo o reuso, provocando novas reflexões para a utilização consciente.'
authors:
- Flávia Araújo
- admin
date: 2007-07-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais do XIII Ciência Jovem, 2007](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais do XIII Ciência Jovem, 2007'
projects: []
publication: "*XIII Anais do XIII Ciência Jovem, 2007*"
publication_short: '*Anais do XIII Ciência Jovem, 2007*'
publication_types:
- '1'
publishDate: 2007-07-10T00:00:00Z
slides: ''
summary: 'Propomos algumas práticas com resíduos oriundos da escola, promovendo o reuso, provocando novas reflexões para a utilização consciente.'
tags:
- Garbage
- Environment
- Creative Industry
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
