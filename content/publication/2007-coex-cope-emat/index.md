---
title: 'Educação, Meio Ambiente e Tecnologia: superando paradigmas (des)estruturados'
abstract: 'Propomos um mapa de paradigmas, estruturados e desestruturados, para avaliarmos (trans)disciplinaridade envolvendo Educação, Meio Ambiente e Tecnologia.'
authors:
- admin
date: 2007-05-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais I SEPE/COEX/COPE, 2007](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais I SEPE/COEX/COPE, 2007'
projects: []
publication: "*I Seminário de Ensino, Pesquisa e Extensão (SEPE), 2007*"
publication_short: '*Anais I SEPE/COEX/COPE, 2007*'
publication_types:
- '1'
publishDate: 2007-05-10T00:00:00Z
slides: ''
summary: 'Propomos um mapa de paradigmas, estruturados e desestruturados, para avaliarmos (trans)disciplinaridade envolvendo Educação, Meio Ambiente e Tecnologia.'
tags:
- Environment
- Technologies
- Learning Design
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
