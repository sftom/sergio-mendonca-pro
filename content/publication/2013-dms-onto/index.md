---
title: 'Ontologies in Global Software Development'
abstract: 'In DSD projects, the teams working are dispersed in different locations [1]. When it comes to projects with scattered teams, the limited communication and lack of sharing and knowledge cause some disadvantages: misinterpreted tasks, lack of collective consciousness relating to the work that has been developed, which issues have been brought up, difficulties following the project’s plan and as to take place in a real-time discussion [2]. These challenges have encourage researchers to look for strategies that can help solve these problems, especially, the search for clear, effective information sharing mechanisms, that is essential in this environment. In this context, the use of ontologies as a standard for representing a domain’s concept [3] can bring significant benefits to DSD projects, by allowing a simple share of information among dispersed teams.'
authors:
- Rodrigo Rocha
- Ryan Azevedo
- admin
- Alex Borges Junior
- Catarina Costa
- Silvio Meira
date: 2013-08-08T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Proceedings DMS, 2013](https://dblp.org/db/conf/dms/dms2013.html#Meira13)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Proceedings DMS, 2013'
projects: []
publication: "*Proceedings of the International 19th Conference on Distributed Multimedia Systems, 2013*"
publication_short: '*Proceedings DMS, 2013*'
publication_types:
- '1'
publishDate: 2013-08-08T00:00:00Z
slides: ''
summary: The use of ontologies as a standard for representing a domain’s concept [3] can bring significant benefits to DSD projects, by allowing a simple share of information among dispersed teams.
tags: 
- Software Engineering
- Ontologies
- DSD
url_code: ''
url_dataset: ''
url_pdf: 'https://dblp.org/db/conf/dms/dms2013.html#Meira13'
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
