---
title: 'Elaboração e discussão de exercícios que envolvem diversos conteúdos da disciplina de Química Inorgânica'
abstract: 'Propomos a elaboração de problemas envolvendo a disciplina de Química Inorgânica baseados no cotidiano das pessoas, metodologia Problem-based Learning (PBL).'
authors:
- admin
- Ângela Campos
date: 2002-05-10T00:00:00Z
isbn: ''
issn: ''
doi: ''
featured: false
image:
  placement: 1
  caption: 'Image by [Anais II JEPEX, 2002](){:target="_blank"}'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Image by Anais II JEPEX, 2002'
projects: []
publication: "*II Jornada de Ensino, Pesquisa e Extensão (UFRPE), 2002*"
publication_short: '*Anais II JEPEX, 2002*'
publication_types:
- '1'
publishDate: 2002-05-10T00:00:00Z
slides: ''
summary: 'Propomos a elaboração de problemas envolvendo a disciplina de Química Inorgânica baseados no cotidiano das pessoas, metodologia Problem-based Learning (PBL).'
tags:
- Problem-based Learning
- PBL
- Inorganic Chemistry
url_code: ''
url_dataset: ''
url_pdf: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

---
{{% callout note %}}
Click the *Cite* button above to import publication metadata into your reference management software.
{{% /callout %}}
