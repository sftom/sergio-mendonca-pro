---
title: "Immersive Game Design"
date: 2020-05-15T16:51:14-03:00
draft: false
# external_link: 'https://sergio.mendonca.pro/tag/immersive-game-design'

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags: 
- Immersive Game Design

# Does the project detail page use math formatting?
math: false

image:
  placement: 1
  caption: 'Photo by [Laurens Derks](https://unsplash.com/@flderks) on [Unsplash](https://unsplash.com)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Photo by Laurens Derks on Unsplash'

# Os jogos apresentam engajamento entre pessoas e grupos de pessoas. Estudar e aplicar técnicas, mecânicas, ferramentas para desenvolvimento de jogos. Extensão, 2014-2015, Concluído.
---
