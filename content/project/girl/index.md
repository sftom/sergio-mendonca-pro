---
title: "Girl in Computer Science"
date: 2020-05-15T16:24:01-03:00
draft: false
# external_link: 'https://sergio.mendonca.pro/tag/woman-in-computer-science/'

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags: 
- Woman in Computer Science

# Does the project detail page use math formatting?
math: false

image:
  placement: 1
  caption: 'Photo by [ThisisEngineering RAEng](https://unsplash.com/@thisisengineering) on [Unsplash](https://unsplash.com)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Photo by ThisisEngineering RAEng on Unsplash'

# Em uma turma ingressante de 40 estudantes, menos de 15% são mulheres. Entre docentes e pesquisadores são 25 a 30%. Extensão, 2014-2015, Concluído.
---
