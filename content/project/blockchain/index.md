---
title: "Blockchain Solutions"
date: 2020-05-15T17:01:48-03:00
draft: false
# external_link: 'https://sergio.mendonca.pro/tag/blockchain'

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags: 
- blockchain

# Does the project detail page use math formatting?
math: false

image:
  placement: 1
  caption: 'Photo by [Hitesh Choudhary](https://unsplash.com/@hiteshchoudhary) on [Unsplash](https://unsplash.com/)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Photo by Hitesh Choudhary on Unsplash'

# Blockchain solutions to bring revolutionary trust and transparency to supply chains, global trade, international payments, the world’s food supply and much more. (by [IBM Blockchain Solutions](https://www.ibm.com/blockchain/solutions)).
---
