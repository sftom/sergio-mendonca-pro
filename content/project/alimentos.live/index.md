---
title: "alimentos.live"
date: 2020-06-05T17:01:50-03:00
draft: false
external_link: 'https://alimentos.live'

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags: 
- alimentos
- live

# Does the project detail page use math formatting?
math: false

image:
  placement: 1
  caption: 'Photo by [Gabriel Benois](https://unsplash.com/@gabrielbenois?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Photo by Gabriel Benois on Unsplash'

# Palestras e debates on-line (lives), na área de Engenharia de Alimentos e Tecnologias.
---
