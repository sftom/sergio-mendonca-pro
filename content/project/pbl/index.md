---
title: "Pbl in Computer Science"
date: 2020-05-15T16:24:36-03:00
draft: false
#external_link: 'https://sergio.mendonca.pro/tag/pbl/'

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags: 
- Problem-based Learning

# Does the project detail page use math formatting?
math: false

image:
  placement: 1
  caption: 'Photo by [Priscilla Du Preez](https://unsplash.com/@priscilladupreez) on [Unsplash](https://unsplash.com)'
  focal_point: 'Center'
  preview_only: false
  alt_text: 'Photo by Priscilla Du Preez on Unsplash'

# A aplicação da PBL (Problem Based Learning) como método do ensino de computação para que o estudante perceba propriedades básicas e demandas do mercado. Extensão, 2014-2015, Concluído.
---
