---
# Display name
title: Marcus Teixeira

# Username (this should match the folder name)
authors:
- marcus-teixeira

# Is this the primary user of the site?
superuser: false

# Role/position
role: Full-Stack Developer

# Organizations/Affiliations
organizations:
- name: Intelivix
  url: "http://intelivix.com/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Performance Algorithms, Data Mining, Application Development, Full-Stack Development.

interests:
- Performance Algorithms
- Data Mining
- Application Development
- Full-Stack Development

education:
  courses:
  - course: BSC in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 2019

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:marcusvini211@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/marcus-ferreira-teixeira/
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/8422770381149615

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "marcusvini211@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Marcus Vinícius Ferreira Teixeira is a Full-Stack Developer at Intelivix. He studied BSC in Computer Science at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
