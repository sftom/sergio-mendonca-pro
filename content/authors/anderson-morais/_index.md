---
# Display name
title: Anderson Morais

# Username (this should match the folder name)
authors:
- anderson-morais

# Is this the primary user of the site?
superuser: false

# Role/position
role: Grad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal Rural de Pernambuco
  url: "https://www.ufrpe.br"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Distributed Systems, Object-Oriented Programming, Web and Mobile Development, and Database Management.

interests:
- Distributed Systems
- Object-Oriented Programming
- Web and Mobile Development
- Database Management

education:
  courses:
  - course: MSc in Applied Informatics
    institution: Universidade Federal Rural de Pernambuco
    year: 
  - course: BSC in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 2018

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:anderson.ufrpe@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://linkedin.com/in/anderson-melo-29134ab4/
- icon: orcid
  icon_pack: ai
  link: http://orcid.org/0000-0001-7795-7183
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/4825772150496499

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "anderson.ufrpe@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Anderson Melo de Morais is a Grad Student in Applied Informatics at Universidade Federal Rural de Pernambuco. He studied BSC in Computer Science at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
