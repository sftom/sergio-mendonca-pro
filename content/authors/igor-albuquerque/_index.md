---
# Display name
title: Igor Albuquerque

# Username (this should match the folder name)
authors:
- igor-albuquerque

# Is this the primary user of the site?
superuser: false

# Role/position
role: Software Engineer Intern

# Organizations/Affiliations
organizations:
- name: Elife
  url: "https://elife.com.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Microservices, Full-Stack Developer (Python, Node, Ruby), Mobile Development (React-Native), Software Development, Programming, Database Management, and New Technologies.

interests:
- Microservices
- Full-Stack Developer (Python, Node, Ruby)
- Mobile Development (React-Native)
- Software Development
- Programming
- Database Management
- New Technologies

education:
  courses:
  - course: BSC in Computer Science
    institution: Universidade Federal do Agreste de Pernambuco
    year: 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:igorcezardba@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/igor-albuquerque-233296161
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/9196106493500566
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-6355-6749
- icon: github
  icon_pack: fab
  link: https://github.com/igorcalbuquerque

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "igorcezardba@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
# - Undergrad Students
- Alumni
---

Igor Cezar Albuquerque Ferreira is a Software Engineer Intern at Elife. He studies BSC in Computer Science at Universidade Federal do Agreste de Pernambuco.
