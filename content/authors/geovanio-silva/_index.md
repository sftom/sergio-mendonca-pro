---
# Display name
title: Geovânio Silva

# Username (this should match the folder name)
authors:
- geovanio-silva

# Is this the primary user of the site?
superuser: false

# Role/position
role: Undergrad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal do Agreste de Pernambuco
  url: "http://www.ufape.edu.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Security, Application Development, Programming, Computer Science.

interests:
- Security
- Application Development
- Programming
- Computer Science

education:
  courses:
  - course: Computer Science
    institution: Universidade Federal do Agreste de Pernambuco
    year: 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sgeojose@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: orcid
  icon_pack: ai
  link: http://orcid.org/
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/2372927154813905
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/geovanio-jos%C3%A9-da-silva-554742145/
- icon: github
  icon_pack: fab
  link: https://github.com/GeovanioJose

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "sgeojose@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
# - Undergrad Students
- Alumni
---

Geovânio Silva is an Undergrad Student at Universidade Federal do Agreste de Pernambuco. He studies Computer Science at Universidade Federal do Agreste de Pernambuco.
