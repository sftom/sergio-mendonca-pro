---
# Display name
title: João Silva Júnior

# Username (this should match the folder name)
authors:
- joao-silva-junior

# Is this the primary user of the site?
superuser: false

# Role/position
role: Professor of Computer Science

# Organizations/Affiliations
organizations:
- name: UNINASSAU
  url: "https://www.uninassau.edu.br/"
- name: Compesa
  url: "https://www.compesa.com.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include the Sanitation, Internet of Things, Document Management, and Backup.

interests:
- Sanitation
- Internet of Things
- Document Management
- Backup

education:
  courses:
  - course: PhD in Electrical Engineering
    institution: Universidade Federal de Pernambuco
    year: 
  - course: MSc in Electrical Engineering
    institution: Universidade Federal de Pernambuco
    year: 2017
  - course: Undergraduate Degree in System Analysis and Development
    institution: Universidade Norte do Paraná
    year: 2013

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:joaoferreirape@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://linkedin.com/in/joaoferreirape 
- icon: orcid
  icon_pack: ai
  link: http://orcid.org/0000-0002-9826-3735
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/8904695743376784
- icon: github
  icon_pack: fab
  link: https://github.com/joaoferreirape

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "joaoferreirape@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

João Ferreira da Silva Júnior is a Professor at UNINASSAU and an Information Technology Analyst at Compesa. He is a Grad Student in Electrical Engineering at Universidade Federal de Pernambuco. He studied Systems Analysis and Development (undergraduate degree) at Universidade Norte do Paraná and earned a master’s degree in Electrical Engineering at Universidade Federal de Pernambuco.
