---
# Display name
title: Yasmin Martins

# Username (this should match the folder name)
authors:
- yasmin-martins

# Is this the primary user of the site?
superuser: false

# Role/position
role: Undergrad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal Rural de Pernambuco
  url: "http://uag.ufrpe.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Application Development, Programming.

interests:
- Application Development
- Programming

education:
  courses:
  - course: BSC in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:yluciomartins@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/3126683570047682

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "yluciomartins@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Yasmin Lucio Martins is an Undergrad Student at Universidade Federal Rural de Pernambuco. She studied BSC in Computer Science at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
