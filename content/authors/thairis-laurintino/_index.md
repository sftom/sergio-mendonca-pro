---
# Display name
title: Thairis Laurentino

# Username (this should match the folder name)
authors:
- thairis-laurentino

# Is this the primary user of the site?
superuser: false

# Role/position
role: Grad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal de Santa Catarina
  url: "https://posenq.posgrad.ufsc.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Food Science and Technologies, Chemical Engineering.

interests:
- Food Science and Technologies
- Chemical Engineering

education:
  courses:
  - course: MSc in Chemical Engineering
    institution: Universidade Federal de Santa Catarina
    year: 
  - course: Food Engineering
    institution: Universidade Federal Rural de Pernambuco
    year: 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:thairiskaroline@hotmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/3799157351324953

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "thairiskaroline@hotmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Thaíris Karoline Silva Laurintino is a Grad Student in Chemical Engineering at Universidade Federal de Santa Catarina. She studied Food Engineering at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
