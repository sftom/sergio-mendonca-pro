---
# Display name
title: Vinicius Souza

# Username (this should match the folder name)
authors:
- vinicius-souza

# Is this the primary user of the site?
superuser: false

# Role/position
role: Grad Student

# Organizations/Affiliations
organizations:
- name: Pontifícia Universidade Católica do Rio de Janeiro
  url: "http://www.inf.puc-rio.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Operational Research, Combinatorial Optimization and Metaheuristics.

interests:
- Operational Research
- Combinatorial Optimization
- Metaheuristics

education:
  courses:
  - course: MSc in Informatics
    institution: Pontifícia Universidade Católica do Rio de Janeiro
    year: 
  - course: BSc in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 2019

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:vinicius.fk9@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/9522332800504700

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "vinicius.fk9@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Vinicius Ferreira de Souza is a Grad Student in Informatics at Pontifícia Universidade Católica do Rio de Janeiro. He studied BSc in Computer Science at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
