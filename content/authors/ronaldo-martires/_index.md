---
# Display name
title: Ronaldo Martires

# Username (this should match the folder name)
authors:
- ronaldo-martires

# Is this the primary user of the site?
superuser: false

# Role/position
role: Undergrad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal do Agreste de Pernambuco
  url: "http://www.ufape.edu.br/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Food Science, Programming.

interests:
- Biotechs
- Application Development
- Programming

education:
  courses:
  - course: Food Engineering
    institution: Universidade Federal Rural de Pernambuco
    year: 2022

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:ronaldomartires@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: orcid
  icon_pack: ai
  link: http://orcid.org/0009-0008-3273-5921
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/1725058381919700

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "ronaldomartires@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Undergrad Students
# - Alumni
---

Ronaldo Martires is an Undergrad Student at Universidade Federal do Agreste de Pernambuco. He studies Food Engineering at Universidade Federal do Agreste de Pernambuco.
