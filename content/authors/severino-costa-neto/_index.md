---
# Display name
title: Severino Costa Neto

# Username (this should match the folder name)
authors:
- severino-costa-neto

# Is this the primary user of the site?
superuser: false

# Role/position
role: Application Development Analyst

# Organizations/Affiliations
organizations:
- name: Accenture
  url: "https://www.accenture.com/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Application Development, Full-Stack Development and Microsoft Partner Solutions.

interests:
- Application Development
- Full-Stack Development
- Microsoft Partner Solutions

education:
  courses:
  - course: BSC in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:sjcostaneto@outlook.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://linkedin.com/in/sjcostaneto/ 
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/3810669286993453
- icon: github
  icon_pack: fab
  link: https://github.com/sjcostaneto/

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "sjcostaneto@outlook.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Severino José da Costa Neto is an Application Development Analyst at Accenture. He studied Application Development, Full-Stack Development and Microsoft Partner Solutions.
