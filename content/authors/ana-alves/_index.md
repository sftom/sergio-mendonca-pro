---
# Display name
title: Ana Alves

# Username (this should match the folder name)
authors:
- ana-alves

# Is this the primary user of the site?
superuser: false

# Role/position
role: Grad Student

# Organizations/Affiliations
organizations:
- name: Universidade Federal de Pernambuco
  url: "https://cin.ufpe.br"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include Performance Assessment and Dependability.

interests:
- Performance Assessment and Dependability

education:
  courses:
  - course: MSc in Computer Science
    institution: Universidade Federal de Pernambuco
    year: 
  - course: BSC in Computer Science
    institution: Universidade Federal Rural de Pernambuco
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:anaraqueldemorais@gmail.com'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/anaraquelmorais/
- icon: cv
  icon_pack: ai
  link: http://lattes.cnpq.br/0884838059225037

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "anaraqueldemorais@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---

Ana Raquel de Morais Alves is an Grad Student in Computer Science at Universidade Federal de Pernambuco. She studied BSC in Computer Science at Universidade Federal Rural de Pernambuco, Unidade Acadêmica de Garanhuns.
